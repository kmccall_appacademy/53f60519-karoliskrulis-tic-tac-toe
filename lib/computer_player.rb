class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name = "Bonzo")
    @name = name
  end

  def get_move
    moves = []
    (0..2).each do |x|
      (0..2).each do |y|
        pos = [x, y]
        moves << pos if board.empty?(pos)
      end
    end

    moves.each do |move|
      return move if wins?(move)
    end
    moves.shuffle.last
  end

  def wins?(pos)
    board.place_mark(pos, mark)
    if board.winner == mark
      board.place_mark(pos, nil)
      true
    else
      board.place_mark(pos, nil)
      false
    end
  end

  def display(board)
    @board = board
  end
end
