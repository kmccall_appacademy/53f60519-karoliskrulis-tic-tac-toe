require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :player_one, :player_two, :current_player

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
    player_one.mark = :X
    player_two.mark = :O
  end

  def play
    current_player.display(board)

    until board.over?
      play_turn
    end

    if game_winner
      game_winner.display(board)
      puts "#{game_winner.name} wins!"
    else
      puts "Cat's game"
    end
  end

  def play_turn
    board.place_mark(current_player.get_move, current_player.mark)
    switch_players!
    current_player.display(board)
  end

  def switch_players!
    self.current_player = current_player == player_one ? player_two : player_one
  end

  if $PROGRAM_NAME == __FILE__
    puts "Enter name "
    name1 = gets.chomp.strip
    name2 = gets.chomp.strip
    p1 = HumanPlayer.new(name1)
    if name2 == ("bot" || "")
      p2 = ComputerPlayer.new("bot")
    else
      p2 = HumanPlayer.new(name2)
    end

    new_game = Game.new(p1, p2)
    new_game.play
  end
end
