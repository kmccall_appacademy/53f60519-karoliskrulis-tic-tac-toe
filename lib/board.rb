class Board
  attr_reader :grid



  def initialize
    @grid = [ [nil, nil, nil],
              [nil, nil, nil],
              [nil, nil, nil] ]
  end

  def place_mark(pos, mark)
    x = pos[0]
    y = pos[1]
    grid[y][x] = mark
  end

  def empty?(pos)
    x = pos[0]
    y = pos[1]
    return true if grid[y][x] == nil
    false
  end

  def winner
    if won_row?(:X) || won_col?(:X) || won_dia?(:X)
      return :X
    elsif won_row?(:O) || won_col?(:O) || won_dia?(:O)
      return :O
    else
      nil
    end
  end

  def won_row?(player)
    grid.any? { |row| row.all? { |mark| mark == player } }
  end

  def won_col?(player)
    grid.transpose.any? { |row| row.all? { |mark| mark == player } }
  end

  def won_dia?(player)
    (0...grid.length).all? { |diag| grid[diag][diag] == player } or (0...grid.length).all? {|row| grid[row][(grid.length - 1) - row] == player }
  end

  def over?
    grid.flatten.none? { |pos| pos.nil? } || winner
  end
end
