class HumanPlayer

  attr_accessor :name, :mark

  def initialize(name = "Frankie")
    @name = name
  end

  def get_move
    puts "Where would you like to move?"
    gets.chomp.split(", ").map(&:to_i)
  end

  def display(board)
    @board = board
  end

end
